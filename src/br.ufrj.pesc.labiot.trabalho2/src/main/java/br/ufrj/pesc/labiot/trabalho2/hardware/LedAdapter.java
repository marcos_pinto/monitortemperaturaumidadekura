package br.ufrj.pesc.labiot.trabalho2.hardware;

import java.io.IOException;

import org.eclipse.kura.gpio.GPIOService;
import org.eclipse.kura.gpio.KuraClosedDeviceException;
import org.eclipse.kura.gpio.KuraGPIODeviceException;
import org.eclipse.kura.gpio.KuraGPIODirection;
import org.eclipse.kura.gpio.KuraGPIOMode;
import org.eclipse.kura.gpio.KuraGPIOPin;
import org.eclipse.kura.gpio.KuraGPIOTrigger;
import org.eclipse.kura.gpio.KuraUnavailableDeviceException;
import org.slf4j.Logger;

/*
 * Classe responsável por abstrair o acesso os LEDs do projeto
 * */
public class LedAdapter {
	private String p;
	private Logger logger;
	
	public LedAdapter(String p,  Logger l) {
		this.p = p;
		this.logger = l;
		
	}
	
	/*
	 * Troca o status de um LED configurado em um pino passado como parmetro
	 */
	public void changeState(GPIOService service, boolean on)  {
		
		logger.info("Trocando o estado do LED " + p + " para " + on + "   "+ service);

		KuraGPIOPin pin = service.getPinByName(p, KuraGPIODirection.OUTPUT,KuraGPIOMode.OUTPUT_PUSH_PULL, KuraGPIOTrigger.NONE);		
				
		logger.info("Obtido LED"+pin);
		
		try {
			logger.info("Abrindo porta");
			if(!pin.isOpen())
				pin.open();
			
			logger.info("Setando valor "+on);
			pin.setValue(on);		
			
		} catch (Exception e) {
			logger.info("ERRO NO LED"+e.getMessage());
			e.printStackTrace();
		}
		
	
		
		logger.info("Exiting changeState");
	}	
}


