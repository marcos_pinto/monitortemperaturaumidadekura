package br.ufrj.pesc.labiot.trabalho2;

import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.eclipse.kura.cloudconnection.listener.CloudConnectionListener;
import org.eclipse.kura.cloudconnection.listener.CloudDeliveryListener;
import org.eclipse.kura.cloudconnection.message.KuraMessage;
import org.eclipse.kura.cloudconnection.publisher.CloudPublisher;
import org.eclipse.kura.configuration.ConfigurableComponent;
import org.eclipse.kura.gpio.GPIOService;
import org.eclipse.kura.gpio.KuraClosedDeviceException;
import org.eclipse.kura.gpio.KuraGPIODeviceException;

import org.eclipse.kura.gpio.KuraUnavailableDeviceException;
import org.eclipse.kura.message.KuraPayload;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.ComponentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



import br.ufrj.pesc.labiot.trabalho2.hardware.LedAdapter;
import br.ufrj.pesc.labiot.trabalho2.hardware.SensorAdapter;


/*
 * Classe que representa o bundle de monitoramento de temperatura e umidade.
 * Implementa as interfaces necessarias para a comunica��o com a cloud e herda de ConfigurableComponent
 * para que permita a configura��o das propriedades pelo usu�rio na interface do Kura
 * */
public class MonitorAmbiente implements ConfigurableComponent, CloudConnectionListener, CloudDeliveryListener {
 
    private static final Logger logger = LoggerFactory.getLogger(MonitorAmbiente.class);
    	
     
    private static final String TEMP_THRESHOLD_NAME = "temperature.threshold";
    private static final String HUM_THRESHOLD_NAME = "humidity.threshold";
    private static final String PUBLISH_RATE_PROP_NAME = "publish.rate";

    private final ScheduledExecutorService worker;
    private ScheduledFuture<?> handle;

    private float temperatureThreshold;
    private float humidityThreshold;
    
    private Map<String, Object> properties;
    
    private CloudPublisher cloudPublisher;
    private GPIOService gpioService;
    
    private LedAdapter temperatureLedAdapter = new LedAdapter("GPIO21", logger);
	private LedAdapter humidityLedAdapter = new LedAdapter("GPIO20", logger);
	private static SensorAdapter sensorAdapter = new SensorAdapter(logger);
    
    // ----------------------------------------------------------------
    //
    // Depend�ncias
    //
    // ----------------------------------------------------------------

    public MonitorAmbiente() {
        super();    
        this.worker = Executors.newSingleThreadScheduledExecutor();
    }

    public void setGPIOService(GPIOService gpioService) {
        this.gpioService = gpioService;
    }

    public void unsetGPIOService(GPIOService gpioService) {
        gpioService = null;
    }

    
    public void setCloudPublisher(CloudPublisher cloudPublisher) {
        this.cloudPublisher = cloudPublisher;
        this.cloudPublisher.registerCloudConnectionListener(MonitorAmbiente.this);
        this.cloudPublisher.registerCloudDeliveryListener(MonitorAmbiente.this);
    }

    public void unsetCloudPublisher(CloudPublisher cloudPublisher) {
        this.cloudPublisher.unregisterCloudConnectionListener(MonitorAmbiente.this);
        this.cloudPublisher.unregisterCloudDeliveryListener(MonitorAmbiente.this);
        this.cloudPublisher = null;
    }

    // ----------------------------------------------------------------
    //
    // APIs de ativa��o
    //
    // ----------------------------------------------------------------

    /* Metodo de ativa��o do bundle
     * Respons�vel por fazer o setup do ambiente, como reset dos LEDs
     * */
    protected void activate(ComponentContext componentContext, Map<String, Object> properties) throws InterruptedException, KuraUnavailableDeviceException, KuraClosedDeviceException, IOException, KuraGPIODeviceException {
        logger.info("Activating MonitorAmbiente...");        
        logger.info("Resetando LEDs.:::." + gpioService);
        
        //Alterando o estado dos LEDs para apagado
        temperatureLedAdapter.changeState(gpioService, false);
        humidityLedAdapter.changeState(gpioService, false);    
        
        this.properties = properties;
        for (Entry<String, Object> property : properties.entrySet()) {
            logger.info("Update - {}: {}", property.getKey(), property.getValue());
        }
        
        try {
            doUpdate(false);
        } catch (Exception e) {
            logger.error("Error during component activation", e);
            throw new ComponentException(e);
        }
        logger.info("Activating MonitorAmbiente... Done.");
    }

    /* Desativa��o do bundle*/
    protected void deactivate(ComponentContext componentContext) {
        logger.debug("Deactivating MonitorAmbiente...");

        this.worker.shutdown();

        logger.debug("Deactivating MonitorAmbiente... Done.");
    }

    /* Atualiza��o do bundle.
     * � chamado sempre que um usu�rio atualiza as configura��es de temperatura e umidade
     * */
    public void updated(Map<String, Object> properties) {
        logger.info("Updated MonitorAmbiente...");

        // Armazena as propriedades alteradas pelo usu�rio
        this.properties = properties;
        for (Entry<String, Object> property : properties.entrySet()) {
            logger.info("Update - {}: {}", property.getKey(), property.getValue());
        }

        doUpdate(true);
        logger.info("Updated MonitorAmbiente... Done.");
    }

    // ----------------------------------------------------------------
    //
    // Callback de Methods da Cloud
    //
    // ----------------------------------------------------------------

    @Override
    public void onConnectionLost() {
        // TODO Auto-generated method stub

    }

    @Override
    public void onConnectionEstablished() {
        // TODO Auto-generated method stub

    }

    @Override
    public void onMessageConfirmed(String messageId) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onDisconnected() {
        // TODO Auto-generated method stub

    }

    // ----------------------------------------------------------------
    //
    // Methodos privados
    //
    // ----------------------------------------------------------------

    /**
     * Metodo respons�vel pela atualiza��o dos valores das propriedades do bundle
     */
    private void doUpdate(boolean onUpdate) {
       
        if (this.handle != null) {
            this.handle.cancel(true);
        }

        if (!this.properties.containsKey(HUM_THRESHOLD_NAME)
        		|| !this.properties.containsKey(TEMP_THRESHOLD_NAME)
                || !this.properties.containsKey(PUBLISH_RATE_PROP_NAME)) {
            logger.info(
                    "Update MonitorAmbiente - Sem propriedades obrigat�rias preenchidas");
            return;
        }

       
        this.temperatureThreshold = (Float) this.properties.get(TEMP_THRESHOLD_NAME);
        this.humidityThreshold = (Float) this.properties.get(HUM_THRESHOLD_NAME);
        int pubrate = (Integer) this.properties.get(PUBLISH_RATE_PROP_NAME);
        
        
        this.handle = this.worker.scheduleAtFixedRate(new Runnable() {

            @Override
            public void run() {
                Thread.currentThread().setName(getClass().getSimpleName());
                try {
					doPublish();
				} catch (KuraUnavailableDeviceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (KuraClosedDeviceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (KuraGPIODeviceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        }, 0, pubrate, TimeUnit.SECONDS);
    } 

    /**
     * Metodo respons�vel por fazer as medi��es e publica��o dos valores na nuvem
     * @throws IOException 
     * @throws KuraClosedDeviceException 
     * @throws KuraUnavailableDeviceException 
     * @throws KuraGPIODeviceException 
     */
    private void doPublish() throws KuraUnavailableDeviceException, KuraClosedDeviceException, IOException, KuraGPIODeviceException {
        if (this.cloudPublisher == null) {
            logger.info("N�o foi selecionado um publisher. A medi��o n�o foi iniciada");
            return;
        }        

        if (this.gpioService == null) {
            logger.info("N�o foi selecionado um GPIO Service. A medi��o n�o foi iniciada");
            return;
        }        

          
        
        logger.info("Realizando leitura do sensor");

		// Realiza a leitura do sensor
		InstantMeasure currentMeasure = sensorAdapter.readSensor();

		logger.info("Resultado da leitura: " + currentMeasure.toString());

		// Se as medi��es ultrapassaram os valores limite, acende os LEDs
		boolean temperatureAbove = currentMeasure.getTemperature() >= this.temperatureThreshold;
		boolean humidityAbove = currentMeasure.getHumidity() >= this.humidityThreshold;
		
    	temperatureLedAdapter.changeState(gpioService,temperatureAbove);
		humidityLedAdapter.changeState(gpioService, humidityAbove);
		
        
		//Montagem da mensagem que ser� enviada para a nuvem
        KuraPayload payload = new KuraPayload();
        
        payload.setTimestamp(new Date());

        // Adicionando as m�tricas
        payload.addMetric("temperatureThreshold", this.temperatureThreshold);
        payload.addMetric("humidityThreshold", this.humidityThreshold);
        payload.addMetric("temperatureMeasurement", currentMeasure.getTemperature());
        payload.addMetric("humidityMeasurement", currentMeasure.getHumidity());
                
        KuraMessage message = new KuraMessage(payload);

        // Publica a mensagem
        try {
            this.cloudPublisher.publish(message);
            logger.info("Published message: {}", payload);
        } catch (Exception e) {
            logger.error("Cannot publish message: {}", message, e);
        }
    }
}
