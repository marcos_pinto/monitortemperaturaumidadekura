package br.ufrj.pesc.labiot.trabalho2;

/*Classe que representa uma medi��o*/
public class InstantMeasure {
	public InstantMeasure(float temperature, float humidity) {
		this.temperature = temperature;
		this.humidity = humidity;
	}

	public float getTemperature() {
		return temperature;
	}

	public float getHumidity() {
		return humidity;
	}

	private float temperature;
	private float humidity;

	@Override
	public boolean equals(Object arg) {
		return this.temperature == ((InstantMeasure) arg).temperature
				&& this.humidity == ((InstantMeasure) arg).humidity;

	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return temperature + " : " + humidity;
	}
}