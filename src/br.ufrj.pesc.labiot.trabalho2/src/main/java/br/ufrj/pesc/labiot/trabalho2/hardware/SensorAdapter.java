package br.ufrj.pesc.labiot.trabalho2.hardware;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.slf4j.Logger;

import br.ufrj.pesc.labiot.trabalho2.*;

/*
 * Classe respons�vel por abstrair o acesso ao sensor DHT11 atrav�s de um c�digo Python
 * */
public class SensorAdapter {

	private static String line;
	private static String[] data;
	static float humidity = 0;
	static float temperature = 0;
	private Logger logger; 

	public SensorAdapter(Logger logger) {
		this.logger = logger;
	}

	public InstantMeasure readSensor() {

		Runtime rt = Runtime.getRuntime();
		Process p;
		try {

			// Acesso ao script Python
			p = rt.exec("python /home/pi/pi.py");

			// Leitura do resultado que vem no formato CSV
			BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
			if ((line = bri.readLine()) != null) {
				if (!(line.contains("ERR_CRC") || line.contains("ERR_RNG"))) {
					data = line.split(";");
					temperature = Float.parseFloat(data[0]);
					humidity = Float.parseFloat(data[1]);
				} else
					System.out.println("Data Error");
			}
			else
				logger.info("Falha ao acessar o script de leitura do sensor em /home/pi/pi.py. ");

			bri.close();
			p.waitFor();

		} catch (IOException e) {
			logger.info("Data Error IOException");
			//e.printStackTrace();
		} catch (InterruptedException e) {
			logger.info("Data Error InterruptedException");
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}

		// Retorna a medi��o
		return new InstantMeasure(temperature, humidity);
	}
}
